#include <iostream>
#include <list>
using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}


// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones

    Fecha(int,int);
    int mes();
    int dia();
    bool operator==(Fecha f);
    void incrementar_dia();

    //Completar miembros internos
  private:
    int mes_;
    int dia_;

};
Fecha::Fecha(int mes, int dia):mes_(mes),dia_(dia){};

int Fecha::mes(){
    return mes_;
}

int Fecha::dia(){
    return dia_;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

bool Fecha::operator==(Fecha f) {
    if (f.dia() == dia_ && f.mes()==mes_){
        return true;
    }else{
        return false;
    }

}

void Fecha::incrementar_dia(){
    if (dias_en_mes(mes_)>=(dia_+1)){
        dia_=dia_+1;
    }else{
        dia_=1;
        if (mes_==12){
            mes_=1;
        }else{
            mes_=mes_+1;
        }
    }
}

/*
#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    // Completar iguadad (ej 9)
    return igual_dia;
}
#endif
*/

// Ejercicio 11, 12
// Clase Horario
class Horario {
public:
    // Completar declaraciones funciones

    Horario(int min,int hora);
    int hora();
    int min();
    bool operator==(Horario h);
    bool operator<(Horario h);

    //Completar miembros internos
private:
    int horas_;
    int minutos_;

};

Horario::Horario(int hora, int minutos): horas_(hora),minutos_(minutos) {};

int Horario::hora() {
    return horas_;
}

int Horario::min() {
    return minutos_;
}

bool Horario::operator==(Horario h) {
    if (h.min() == minutos_ && h.hora()==horas_){
        return true;
    }else{
        return false;
    }

}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator<(Horario h) {
// Completar
    if ( h.hora()>horas_){
        return true;
    }else{
        if (h.min() > minutos_ && h.hora()==horas_){
            return true;
        }else {
            return false;
        }
    }

}


// Ejercicio 13

// Clase Recordatorio
class Recordatorio {
public:
    // Completar declaraciones funciones

    Recordatorio(Fecha fechaa,Horario horarioo,string detallee);
    map<Fecha,pair<Horario,string>> recordatorio(Fecha f);
    Fecha fecha();
    Horario horario();
    string detalle();
    bool operator<(Recordatorio); // lo uso en agenda para ordenar los recordatorios

    //Completar miembros internos
private:
    Fecha fecha_;
    Horario horario_;
    string detalle_;

};

Recordatorio::Recordatorio(Fecha fecha,Horario horario,string detalle):fecha_(fecha),horario_(horario),detalle_(detalle){};

Fecha Recordatorio::fecha(){return fecha_;}
Horario Recordatorio::horario(){return horario_;}
string Recordatorio::detalle(){return detalle_;}



ostream& operator<<(ostream& os, Recordatorio R) {
    os << R.detalle() <<" @ "<< R.fecha()<<" "<<R.horario();
    return os;
}

// Ejercicio 14

bool Recordatorio::operator<(Recordatorio r) {
    if (this->horario_<r.horario() ){
        return true;
    }else{
        return false;
    }

}


// Clase Agenda

class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    list<Recordatorio> recordatorios_de_hoy_;
    Fecha hoy_;
};

Agenda::Agenda(Fecha fecha_inicial):hoy_(fecha_inicial),recordatorios_de_hoy_(){

}

void Agenda::agregar_recordatorio(Recordatorio rec){

        recordatorios_de_hoy_.push_back(rec);
        recordatorios_de_hoy_.sort();

}
Fecha Agenda::hoy(){
    return hoy_;
}

void Agenda::incrementar_dia() {

    hoy_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy(){
    return recordatorios_de_hoy_;
}

ostream& operator<<(ostream& os, Agenda A) {
    os << A.hoy()<<"\n";
    os <<"====="<<"\n";
    for (Recordatorio r : A.recordatorios_de_hoy()){
        if (r.fecha()==A.hoy()) {
            os << r << "\n";
        }
    }
    return os;
}
